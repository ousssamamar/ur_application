#include <ros/ros.h>
#include <std_msgs/Float64MultiArray.h>
#include <sensor_msgs/JointState.h>
#include <umrob/robot.h>
#include <umrob/robot_model.h>
#include <umrob/robot_controller.h>
#include <Eigen/Dense>

double Mat[3][3]={0};
void JointCallback(const sensor_msgs::JointState::ConstPtr& msg){
    double pose=msg->position;
    double vele=msg->velocity;
    double accele=msg->effort;
    Mat[0][0]=pose[0];
    Mat[0][1]=pose[1];
    Mat[0][2]=pose[2];

    Mat[1][0]=vele[0];
    Mat[1][1]=vele[1];
    Mat[1][2]=vele[2];

    Mat[2][0]=accele[0];
    Mat[2][1]=accele[1];
    Mat[2][2]=accele[2];

}
    

int main(int argc, char** argv){
    ros::init(argc, argv, "control");
    ros::NodeHandle n;
    ros::Subscriber sub = n.subscribe("left_arm_controller/command",50,JointCallback);
    ros::spin();

    constexpr double time_step = 0.01;
    auto model1 = umrob::RobotModel(left_arm_controller);
    auto model2 = umrob::RobotModel(right_arm_controller);
    auto robot1 = umrob::Robot(model1);
    auto robot2 = umrob::Robot(model2);
    auto controller1 = umrob::RobotController(robot1, time_step);
    auto controller2 = umrob::RobotController(robot2, time_step);
// left
    const Eigen::Index j1_idx = model1.jointIndex("left_elbow_joint"); 
    const Eigen::Index j2_idx = model1.jointIndex("left_shoulder_lift_joint");
    const Eigen::Index j3_idx = model1.jointIndex("left_shoulder_pan_joint");
    const Eigen::Index j4_idx = model1.jointIndex("left_wrist_1_joint");
    const Eigen::Index j5_idx = model1.jointIndex("left_wrist_2_joint");
    const Eigen::Index j6_idx = model1.jointIndex("left_wrist_3_joint");
// right
    const Eigen::Index j1_idx = model2.jointIndex("right_elbow_joint");
    const Eigen::Index j2_idx = model2.jointIndex("right_shoulder_lift_joint");
    const Eigen::Index j3_idx = model2.jointIndex("right_shoulder_pan_joint");
    const Eigen::Index j4_idx = model2.jointIndex("right_wrist_1_joint");
    const Eigen::Index j5_idx = model2.jointIndex("right_wrist_2_joint");
    const Eigen::Index j6_idx = model2.jointIndex("right_wrist_3_joint");



    auto& cp = robot.controlPoint("tcp");
    cp.target.position.x()= Mat[0][0];
    cp.target.position.y()= Mat[0][1];   
    cp.target.position.z()= Mat[0][2];

    cp.target.velocity.x()= Mat[1][0];
    cp.target.velocity.x()= Mat[1][1];
    cp.target.velocity.x()= Mat[1][2];

    cp.target.acceleration.x()= Mat[2][0];
    cp.target.acceleration.x()= Mat[2][1];
    cp.target.acceleration.x()= Mat[2][2];
    

    controller.addControlPointVelocityTask("tcp", 1.);
    controller.addJointsVelocityConstraint();

    controller.print();

    controller.reset();

    auto left_pos_pub = node.advertise<std_msgs::Float64MultiArray>(
        "/left_arm_controller/command", 1);

    auto right_pos_pub = node.advertise<std_msgs::Float64MultiArray>(
        "/right_arm_controller/command", 1);

    std_msgs::Float64MultiArray left_cmd;
    left_cmd.data.resize(6, 0.);

    std_msgs::Float64MultiArray right_cmd;
    right_cmd.data.resize(6, 0.);

    left_pos_pub.publish(data);
    right_pos_pub.publish(data);

    ros::spinOnce();

    rate.sleep();

    return 0;
}

