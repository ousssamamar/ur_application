#include <ros/ros.h>
#include <tf2_msgs/TFMessage.h>
#include <umrob/trajectory_generator.h>
#include <std_msgs/Float64MultiArray.h>
#include <fmt/format.h>
#include <fmt/color.h>
#include <fmt/ostream.h>
#include <thread>
#include <chrono>


std::vector<double>left_right_vec;

void JointCallback(const geometry_msgs::TransformStamped::ConstPtr& msg){

//on remplis le vecteur left_right_vec par les valeurs recues depuis l'application pour les deux robots
if(left_right_vec.size()<=18)
{left_right_vec.push_back(msg->transform.translation.x) ;
left_right_vec.push_back(msg->transform.translation.y) ;
left_right_vec.push_back(msg->transform.translation.z) ;
 }
 if(left_right_vec.size()>=18)
 {   for(int i=0;i<9;i++)
    ROS_INFO("les points recues pour le robot de droite sont(%f \n)",left_right_vec[i]);
    for(int i=9;i<18;i++)
    ROS_INFO("les points recues pour le robot de la gauche sont(%f \n)",left_right_vec[i]);
 }
 }

 int main(int argc, char** argv)
 {
    ros::init(argc, argv, "Trajectoire");
    ros::NodeHandle node;
    ros::Subscriber sub = node.subscribe("/tf",50,JointCallback); 
 
    constexpr auto time_step = 0.05;
    umrob::TrajectoryGenerator trajectory_right(time_step);
    umrob::TrajectoryGenerator trajectory_left(time_step);


    Eigen::Vector6d vmax = Eigen::Vector6d::Ones();
    Eigen::Vector6d amax = Eigen::Vector6d::Ones();
    auto pose = Eigen::Affine3d::Identity();

    auto P =left_right_vec;

/////////////////les points recus de l'application (robot de droite)//////////////////////
    pose.translation() << P[0],P[1],P[2];
    trajectory_right.startFrom(pose);

    pose.translation() << P[3],P[4],P[5] ;
    trajectory_right.addWaypoint(pose, vmax, amax);

    pose.translation() << P[6],P[7],P[8];
    trajectory_right.addWaypoint(pose, 1.);


/////////////////les points recus de l'application (robot de gauche)//////////////////////

    pose.translation() << P[9],P[10],P[11];
    trajectory_left.startFrom(pose);

    pose.translation() << P[12],P[13],P[14] ;
    trajectory_left.addWaypoint(pose, vmax, amax);

    pose.translation() << P[15],P[16],P[17];
    trajectory_left.addWaypoint(pose, 10.);


    fmt::print("Right Trajectory length: {}\n", trajectory_right.duration()); 
    fmt::print("left Trajectory length: {}\n", trajectory_left.duration()); 

    ros::Publisher pub = node.advertise<std_msgs::Float64MultiArray>("/traj",100) ; 
    std_msgs::Float64MultiArray Posd_right; 
    std_msgs::Float64MultiArray Veld_right;
    std_msgs::Float64MultiArray Accd_right;
    Posd_right.data.resize(3,0.);
    Veld_right.data.resize(3,0.);
    Accd_right.data.resize(3,0.);

    std_msgs::Float64MultiArray Posd_left; 
    std_msgs::Float64MultiArray Veld_left;
    std_msgs::Float64MultiArray Accd_left;
    Posd_left.data.resize(3,0.);
    Veld_left.data.resize(3,0.);
    Accd_left.data.resize(3,0.);

    double print_timer{0};
    const double sample_time = 0.01;
    ros::Rate rate(1. / sample_time);
    while (((trajectory_right.state()!=umrob::TrajectoryGenerator::State::TrajectoryCompleted)
     && (trajectory_left.state()!=umrob::TrajectoryGenerator::State::TrajectoryCompleted))  &&
      ros::ok() ) {
        if (trajectory_right.update() ==
            umrob::TrajectoryGenerator::State::WaypointReached) {
            fmt::print(fg(fmt::color::red) | fmt::emphasis::bold, "\n{:*^50}\n",
                       " Waypoint reached ");}
        if (trajectory_left.update() ==
            umrob::TrajectoryGenerator::State::WaypointReached) {
            fmt::print(fg(fmt::color::blue) | fmt::emphasis::bold, "\n{:*^50}\n",
                       " Waypoint reached ");}
        print_timer += time_step;
        if (print_timer > 0.5) {
            print_timer = 0;
            for(int i=0;i<3;i++)
               { Posd_right.data[i]=trajectory_right.targetPose().translation()[i];
                Veld_right.data[i]=trajectory_right.targetVelocity().head<3>()[i];
                Accd_right.data[i]=trajectory_right.targetAcceleration().head<3>()[i];
                Posd_left.data[i]=trajectory_left.targetPose().translation()[i];
                Veld_left.data[i]=trajectory_left.targetVelocity().head<3>()[i];
                Accd_left.data[i]=trajectory_left.targetAcceleration().head<3>()[i];}
           pub.publish(Posd_right);
           pub.publish(Posd_left);
           pub.publish(Veld_right);
           pub.publish(Veld_left);
           pub.publish(Accd_right);
           pub.publish(Accd_left);
        }
        std::this_thread::sleep_for(std::chrono::duration<double>(time_step));
        ros::spinOnce();
        rate.sleep();
     }
     fmt::print(fg(fmt::color::red) | fmt::emphasis::bold, "\n{:*^50}\n",
               " Trajectoires des deux bras completees ");
 }
   