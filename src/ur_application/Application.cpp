#include <ros/ros.h>
#include <tf2_msgs/TFMessage.h>



geometry_msgs::TransformStamped coll_objects;

void collObjects(){
 // std::vector<geometry_msgs::TransformStamped> coll_objects;
  
  /* coll_objects[0].id = "Objet";
  coll_objects[0].header.frame_id = "unit_box";

  coll_objects[0].dimensions.resize(3);
  coll_objects[0].dimensions[0] = 1;
  coll_objects[0].dimensions[1] = 1;
  coll_objects[0].dimensions[2] = 1; */
  coll_objects.transform.translation.x = -0.070151;
  coll_objects.transform.translation.y = 1.368400;
  coll_objects.transform.translation.z = 0.5;
}


int main(int argc, char* argv[]) {
    ros::init(argc, argv, "Application");
    ros::NodeHandle node;
   
    auto pub_OT_left = node.advertise<geometry_msgs::TransformStamped>("/tf",100);
    
    std::vector<geometry_msgs::TransformStamped> point_left;
    point_left.resize(3);
    // organe terminale gauche 
    point_left[0].transform.translation.x=0.184276;
    point_left[0].transform.translation.y=0.163430;
    point_left[0].transform.translation.z=1.010628;
    //Point de collision surface gauche 
    point_left[1].transform.translation.x=-0.070151-0.5;
    point_left[1].transform.translation.y=1.368400;
    point_left[1].transform.translation.z=0.5;
    //Point final guache 
    point_left[2].transform.translation.x=0,491045-0.5;
    point_left[2].transform.translation.y=-0,564206;
    point_left[2].transform.translation.z=0.499986;

    
    auto pub_OT_right = node.advertise<geometry_msgs::TransformStamped>("/tf",100);
    std::vector<geometry_msgs::TransformStamped> point_right;
    point_right.resize(3);
    // organe terminale droit
    point_right[0].transform.translation.x=2.184280;
    point_right[0].transform.translation.y=0.163407;
    point_right[0].transform.translation.z=1.010178;
    //Point de collision surface droit
    point_right[1].transform.translation.x=-0.70151+0.5;
    point_right[1].transform.translation.y=1.368400;
    point_right[1].transform.translation.z=0.5;
    //Point final droit 
    point_right[2].transform.translation.x=0,491045+0.5;
    point_right[2].transform.translation.y=-0,564206;
    point_right[2].transform.translation.z=0.499986;

    ros::Rate rate(10);
    //publier les donnees au noeud trajectoire 
    while (ros::ok()) {
    pub_OT_left.publish(point_left[0]);
    pub_OT_left.publish(point_left[1]);
    pub_OT_left.publish(point_left[2]);

    pub_OT_right.publish(point_right[0]);
    pub_OT_right.publish(point_right[1]);
    pub_OT_right.publish(point_right[2]);
        ros::spinOnce();

        rate.sleep();

    }
}