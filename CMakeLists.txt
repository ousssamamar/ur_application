cmake_minimum_required(VERSION 3.0.2)
project(ur_application)

find_package(catkin REQUIRED COMPONENTS 
    message_generation
    roscpp
    std_msgs
    sensor_msgs 
    tf2_msgs
)

include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
 conan_basic_setup(TARGETS)

################################################
## Declare ROS messages, services and actions ##
################################################

###################################
## catkin specific configuration ##
###################################
## The catkin_package macro generates cmake config files for your package
## Declare things to be passed to dependent projects
## INCLUDE_DIRS: uncomment this if your package contains header files
## LIBRARIES: libraries you create in this project that dependent projects also need
## CATKIN_DEPENDS: catkin_packages dependent projects also need
## DEPENDS: system dependencies of this project that dependent projects also need
catkin_package(
    CATKIN_DEPENDS 
        roscpp
        std_msgs
        sensor_msgs
        tf2_msgs
)

###########
## Build ##
###########

add_executable(
    ${PROJECT_NAME}_ur_application 
    src/ur_application/Application.cpp
)

target_include_directories(
    ${PROJECT_NAME}_ur_application 
    PRIVATE
        include
        ${catkin_INCLUDE_DIRS}
)

target_link_libraries(
    ${PROJECT_NAME}_ur_application
    PRIVATE
        ${catkin_LIBRARIES}
        CONAN_PKG::robot-control
        CONAN_PKG::robot-model
        CONAN_PKG::robot-trajectory
)

target_compile_features(${PROJECT_NAME}_ur_application PUBLIC cxx_std_17)

add_dependencies(${PROJECT_NAME}_ur_application ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
set_target_properties(${PROJECT_NAME}_ur_application PROPERTIES OUTPUT_NAME ur_application PREFIX "" INSTALL_RPATH "${CONAN_LIB_DIRS}")
# add_dependencies(${PROJECT_NAME}_ur_application ${PROJECT_NAME}_generate_messages_cpp)

#############
## Install ##
#############

install(
    TARGETS ${PROJECT_NAME}_ur_application
    RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

install(
    DIRECTORY launch urdf controller
    DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
)

# Force the generation of a compile_commands.json file to provide autocompletion for IDEs
set(CMAKE_EXPORT_COMPILE_COMMANDS TRUE CACHE BOOL "" FORCE)
